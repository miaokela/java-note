![1707230964384](image/README/1707230964384.png)

## Java简易之路

[Spring Boot开发简易流程](https://gitee.com/miaokela/java-note/blob/master/springboot.md)

[Spring Cloud开发简易流程](https://gitee.com/miaokela/java-note/blob/master/spring-cloud.md)

[Spring Boot整合JWT](https://gitee.com/miaokela/java-note/blob/master/springsecurity.md)

[Spring Boot整合Redis](https://gitee.com/miaokela/java-note/blob/master/springboot%E8%B6%A3%E5%91%B3%E5%AE%9E%E6%88%98%E8%AF%BE-%E7%AC%94%E8%AE%B0/%E9%9B%86%E6%88%90redis.md)

[Spring Boot整合Swagger](https://gitee.com/miaokela/java-note/blob/master/springboot%E8%B6%A3%E5%91%B3%E5%AE%9E%E6%88%98%E8%AF%BE-%E7%AC%94%E8%AE%B0/swagger.md)

[统一响应结构与全局异常](https://gitee.com/miaokela/java-note/blob/master/%E5%85%A8%E5%B1%80%E5%BC%82%E5%B8%B8%E5%A4%84%E7%90%86_%E7%BB%9F%E4%B8%80%E5%93%8D%E5%BA%94%E7%BB%93%E6%9E%84.md)

[参数校验](https://gitee.com/miaokela/java-note/blob/master/springboot%E8%B6%A3%E5%91%B3%E5%AE%9E%E6%88%98%E8%AF%BE-%E7%AC%94%E8%AE%B0/%E5%8F%82%E6%95%B0%E6%A0%A1%E9%AA%8C.md)

[自定义拦截器](https://gitee.com/miaokela/java-note/blob/master/springboot%E8%B6%A3%E5%91%B3%E5%AE%9E%E6%88%98%E8%AF%BE-%E7%AC%94%E8%AE%B0/%E8%87%AA%E5%AE%9A%E4%B9%89%E6%8B%A6%E6%88%AA%E5%99%A8.md)
