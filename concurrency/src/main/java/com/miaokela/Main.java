package com.miaokela;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.miaokela.utils.MyRunnable;
import com.miaokela.utils.MyThread;

public class Main {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        // 1 Thread
        MyThread myThread = new MyThread();
        myThread.start();
        myThread.join();

        // 2 Runnable
        Thread t = new Thread(new MyRunnable());
        t.start();
        t.join();

        // 3 ExecutorService
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        newSingleThreadExecutor.execute(new Runnable() {  // 匿名内部类实现Runnable 语法糖
            public void run() {
                System.out.println("通过ExecutorService运行线程");
            }
        });
        newSingleThreadExecutor.shutdown();

        // 4 Future 和 Callable
        ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
        Callable<String> task = new Callable<String>() {
            public String call() {
                return "通过Future和Callable获取结果";
            }
        };
        Future<String> future = newCachedThreadPool.submit(task);
        System.out.println(future.get());
        newCachedThreadPool.shutdown();

        // 5 CompletableFuture
        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> "通过CompletableFuture获取结果");
        System.out.println(future2.get());
    }
}