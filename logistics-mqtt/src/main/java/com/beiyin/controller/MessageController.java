package com.beiyin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.beiyin.pojo.MessageRequest;
import com.beiyin.service.MessageSendingService;


@RestController
public class MessageController {

    private final MessageSendingService messageSendingService;

    @Autowired
    public MessageController(MessageSendingService messageSendingService) {
        this.messageSendingService = messageSendingService;
    }

    @PostMapping("/send-message")
    public String sendMessage(@RequestBody MessageRequest request) {
        messageSendingService.sendMessage(request.getTopic(), request.getPayload());
        return "Message sent successfully";
    }
}
