package com.beiyin.service;

import org.springframework.stereotype.Service;

import com.beiyin.pojo.DeviceData;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MqttMessageHandler {

    // 这个方法用于处理接收到的 MQTT 消息
    public void handleMessage(String payload) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            DeviceData deviceData = objectMapper.readValue(payload, DeviceData.class);
            // 现在 deviceData 对象包含了 JSON 数据，可以按需使用
            System.out.println("Received MQTT message with flag: " + deviceData.getFlag());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
