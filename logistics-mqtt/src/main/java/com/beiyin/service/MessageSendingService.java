package com.beiyin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

@Service
public class MessageSendingService {

    private final MessageChannel mqttOutboundChannel;

    @Autowired
    public MessageSendingService(MessageChannel mqttOutboundChannel) {
        this.mqttOutboundChannel = mqttOutboundChannel;
    }

    public void sendMessage(String topic, String payload) {
        // 创建消息，并设置主题
        Message<String> message = MessageBuilder.withPayload(payload)
                                                .setHeader("mqtt_topic", topic)
                                                .build();
        // 发送消息
        this.mqttOutboundChannel.send(message);
    }
}
