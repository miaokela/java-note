package com.beiyin.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;


@Data
public class DeviceData {
    private int flag;
    private long deviceid;
    private int recv_result;
    @JsonProperty("CSQ")
    private int csq;
    private String recv_type;
}
