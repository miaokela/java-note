package com.miaokela;


import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


@SpringCloudApplication
@EnableFeignClients  // 开启Feign功能
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    /**
     * Spring提供的RestTemplate模板工具类，封装了HTTP客户端，并且实现了对象与json的
     * 序列化和反序列化
     * @return
     */
    @Bean
    @LoadBalanced  // 消费者提供负载均衡器
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}