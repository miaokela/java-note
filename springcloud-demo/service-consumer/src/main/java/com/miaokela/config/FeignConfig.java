package com.miaokela.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;

@Configuration
public class FeignConfig {

    /**
     * NONE: 不记录任何日志信息，默认值
     * BASIC: 仅记录请求的方法，URL、响应状态码和执行时间
     * HEADERS: 在BASIC基础之上，记录请求和响应头的信息
     * FULL: 记录所有请求和响应的明细，包括头信息、请求体、元数据
     * @return
     */
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

}
