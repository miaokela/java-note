package com.miaokela.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.miaokela.client.fallback.UserClientFallback;
import com.miaokela.config.FeignConfig;
import com.miaokela.consumer.pojo.User;

@FeignClient(value = "service-provider", fallback=UserClientFallback.class, configuration = FeignConfig.class)
public interface UserClient {
    @GetMapping("/user/{id}")
    User queryById(@PathVariable("id") Long id);
}
