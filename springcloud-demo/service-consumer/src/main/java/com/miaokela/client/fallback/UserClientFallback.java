package com.miaokela.client.fallback;

import org.springframework.stereotype.Component;

import com.miaokela.client.UserClient;
import com.miaokela.consumer.pojo.User;


@Component
public class UserClientFallback implements UserClient {

    @Override
    public User queryById(Long id) {
        User user = new User();
        user.setId(id);
        user.setName("用户异常");
        return user;
    }
}
