CREATE TABLE `tb_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tb_user (username, password, name, age, sex, birthday, created, updated, note)
VALUES ('user1', 'password1', '张三', 25, 1, NOW(), NOW(), NOW(), '备注信息1'),
       ('user2', 'password2', '李四', 30, 0, NOW(), NOW(), NOW(), '备注信息2'),
       ('user3', 'password3', '王五', 35, 1, NOW(), NOW(), NOW(), '备注信息3');
