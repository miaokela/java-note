
### 引入依赖

![1707144008825](image/集成redis/1707144008825.png)

### 添加配置

![1707144144161](image/集成redis/1707144144161.png)

### 创建配置类

![1707190976108](image/集成redis/1707190976108.png)

### 使用RedisTemplate

![1707191014715](image/集成redis/1707191014715.png)

### 代码：

```yaml
spring:
  redis:
    host: 127.0.0.1
    port: 6379
    password: 123456
    timeout: 1000

server:
  port: 8003
```

```java
package com.miaokela.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);

        // 设置序列化工具，这里使用JSON序列化
        Jackson2JsonRedisSerializer<Object> jacksonSeial = new Jackson2JsonRedisSerializer<>(Object.class);

        // 配置RedisTemplate
        template.setValueSerializer(jacksonSeial);
        template.setHashValueSerializer(jacksonSeial);
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());

        template.afterPropertiesSet();

        return template;
    }
}
```

```java
package com.miaokela.controller;


import com.miaokela.utils.ApiResponse;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HelloController {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public void setKey(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public Object getKey(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    @GetMapping("/hello")
    public ApiResponse<String> hello()  {
        log.info("测试一下");
        this.setKey("name", "miaokela");
        Object key = this.getKey("name");
        System.out.println(key);

        return new ApiResponse<>(HttpStatus.OK.value(), "操作成功", "Hello World!");
    }

}
```
