package com.miaokela.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.miaokela.mapper.UserMapper;
import com.miaokela.pojo.User;
import com.miaokela.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
