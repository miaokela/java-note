package com.miaokela.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.miaokela.pojo.User;

/**
 * 用以Service CRUD接口
 */
public interface UserService extends IService<User> {
}
