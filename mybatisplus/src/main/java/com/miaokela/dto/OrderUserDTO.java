package com.miaokela.dto;

import lombok.Data;

@Data
public class OrderUserDTO {
    private Long orderId;
    private String orderDetails;
    private Long userId;
    private String userName;
}
