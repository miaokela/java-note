package com.miaokela.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.miaokela.pojo.User;

/**
 * 用以Mapper CRUD接口
 */
public interface UserMapper extends BaseMapper<User> {

}
