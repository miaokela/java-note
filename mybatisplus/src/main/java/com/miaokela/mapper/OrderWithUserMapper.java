package com.miaokela.mapper;


import com.miaokela.dto.OrderUserDTO;

public interface OrderWithUserMapper {
    OrderUserDTO selectOrderWithUser(Long orderId);
}
