

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.miaokela.dto.OrderUserDTO;
import com.miaokela.mapper.OrderWithUserMapper;
import com.miaokela.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.miaokela.Application;
import com.miaokela.mapper.UserMapper;
import com.miaokela.pojo.User;

@SpringBootTest(classes = Application.class)
public class SampleTest {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderWithUserMapper orderWithUserMapper;

    @Test
    public void testSelect() {
        System.out.println("测试...");
        List<User> list = userMapper.selectList(null);
        System.out.println(list);
        list.forEach(item -> {
            System.out.println("Name:" + item.getName());
        });
    }

    @Test
    public void testInsert() {
        User user = new User();
        user.setName("John Doe");
        user.setAge(30);
        user.setEmail("john.doe@example.com");
        userMapper.insert(user);
    }

    @Test
    public void testUpdate() {
        User userToUpdate = userMapper.selectById(1L);
        userToUpdate.setEmail("new.email@example.com");
        userMapper.updateById(userToUpdate);
    }

    @Test
    public void testSelectById() {
        User user = userMapper.selectById(1L);
        System.out.println(user);
    }

    @Test
    public void deleteById() {
        userMapper.deleteById(1L);
    }

    /**
     * IService
     */
    @Test
    public void testInsertService() {
        User user = new User();
        user.setName("John Doe");
        user.setAge(30);
        user.setEmail("john.doe@example.com");
        userService.save(user);
    }

    @Test
    public void testDeleteService() {
        userService.removeById(1L);
    }

    @Test
    public void testUpdateService() {
        User userToUpdate = userService.getById(2L);
        userToUpdate.setEmail("new.email@example.com");
        userService.updateById(userToUpdate);
    }

    @Test
    public void testSelectService() {
        List<User> list = userService.list();
        System.out.println(list);
        list.forEach(item -> {
            System.out.println("Name:" + item.getName());
        });
    }

    @Test
    public void testSelectByIdService() {
        User user = userService.getById(2L);
        System.out.println(user);
    }
    /**
     * QueryWrapper
     */
    @Test
    public void testQueryWrapper() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", "John Doe")
                .between("age", 20, 30)
                .isNotNull("email");
        List<User> users = userMapper.selectList(queryWrapper);
        System.out.println(users);
    }

    /**
     * UpdateWrapper
     */
    @Test
    public void testUpdateWrapper() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("email", "new.email@example.com")
                .eq("name", "John Doe")
                .between("age", 20, 30)
                .isNotNull("email");
        int rows = userMapper.update(null, updateWrapper);
        System.out.println(rows + " rows updated");
    }

    /**
     * Wrapper自定义SQL
     */
    @Test
    public void testWrapper() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.apply("name = {0}", "John Doe");
        List<User> users = userMapper.selectList(queryWrapper);
        System.out.println(users);
    }

    /**
     * LambdaQueryWrapper
     */
    @Test
    public void testLambdaQueryWrapper() {
        LambdaQueryWrapper<User> userLambdaQueryWrapper = Wrappers.lambdaQuery(User.class);
        userLambdaQueryWrapper.eq(User::getName, "John Doe")
                .between(User::getAge, 20, 30)
                .isNotNull(User::getEmail);
        List<User> users = userMapper.selectList(userLambdaQueryWrapper);
        System.out.println(users);
    }

    /**
     * LambdaUpdateWrapper
     */
    @Test
    public void testLambdaUpdateWrapper() {
        LambdaUpdateWrapper<User> userLambdaUpdateWrapper = Wrappers.lambdaUpdate(User.class);
        userLambdaUpdateWrapper.set(User::getEmail, "new.email@example.com")
                .eq(User::getName, "John Doe")
                .between(User::getAge, 20, 30)
                .isNotNull(User::getEmail);
        int rows = userMapper.update(null, userLambdaUpdateWrapper);
        System.out.println(rows + " rows updated");
    }

    /**
     * 复杂的查询 比如连表
     */
    @Test
    public void testSelectWithOrder() {
        OrderUserDTO orderUserDTO = orderWithUserMapper.selectOrderWithUser(1L);
        System.out.println(orderUserDTO);
    }
}
