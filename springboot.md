## springboot开发流程

### 1 创建项目

### 2 创建工程

### 3 基本流程

##### 添加父工程坐标

![1706834774561](image/springboot/1706834774561.png)

##### 添加web启动器

![1706835055775](image/springboot/1706835055775.png)

##### 指定maven工程jdk版本

![1706835148218](image/springboot/1706835148218.png)

##### 编写启动类

![1706835378467](image/springboot/1706835378467.png)

##### 编写controller

![1706835363839](image/springboot/1706835363839.png)

##### 启动测试

![1706836514569](image/springboot/1706836514569.png)

##### 附：配置

```xml
    <properties>
        <java.verison>1.8</java.verison>
    </properties>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.12.RELEASE</version>
    </parent>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
```

### 整合MyBatis

##### 添加依赖

![1706837513455](image/springboot/1706837513455.png)

##### 创建Application.properties文件

![1706838764429](image/springboot/1706838764429.png)

##### 添加配置类

![1706838539870](image/springboot/1706838539870.png)

##### 创建实体类

![1706838270471](image/springboot/1706838270471.png)

##### 创建Dao接口

![1706838855596](image/springboot/1706838855596.png)

##### 添加映射mapper文件

![1706839676116](image/springboot/1706839676116.png)

##### 添加MapperScan注解扫描

![1706839716247](image/springboot/1706839716247.png)

##### 测试代码

![1706840172053](image/springboot/1706840172053.png)

### 整合tk.mybatis

> 避免重复CRUD映射的框架

##### 添加依赖

![1706840399564](image/springboot/1706840399564.png)

##### 修改实体类

> tk.mybatis 实体类使用的注解是jpa注解

```
1. 默认表名就是类名，字段名就是属性名
2. @Table(name="")指定表名
3. @Column(name="")指定字段名
4. @Teansient 注解表示不与字段映射
```

![1706842153073](image/springboot/1706842153073.png)

##### 修改dao，使用简化方法

> 不需要任何配置，就可以使用

![1706842039010](image/springboot/1706842039010.png)

> 执行测试

![1706842200251](image/springboot/1706842200251.png)

##### 修改注解扫描

![1706841595433](image/springboot/1706841595433.png)

##### 映射复杂方法

> 修改dao

![1706842452279](image/springboot/1706842452279.png)

> 自定义映射文件

![1706842588576](image/springboot/1706842588576.png)

> 测试

![1706843445663](image/springboot/1706843445663.png)

##### tk.mybatis常用方法介绍

> Select

```java
List<T> select(T record);
T selectByPrimaryKey(Object key);
List<T> selectAll();
T selectOne(T record);
int selectCount(T record);
```

> Insert

```java
int insert(T record); 保存尸体，null也会保存，不适用数据库默认值
int insertSelective(T record); 保存实体，null不会保存，会使用数据库默认值
```

> Update

```java
int updateByPrimaryKey(T record); 根据主键更新 null值也更新
int updateByPrimaryKeySelective(T record); 根据主键更新属性不为null的值
```

> Delete

```java
int delete(T record); 根据实体属性作为条件删除
int deleteByPrimaryKey(Object key); 根据主键删除
```

> Example方法

```java
List<T> selectByExample(Object example);
int selectCountByExample(Object example);
int updateByExample(@Param("record") T record, @Param("example") Object example);
int updateByExampleSelective(@Param("record") T record, @Param("example") Object example);
int deleteByExample(Object example);
```

> example示例

![1706844142948](image/springboot/1706844142948.png)

### 整合Mybatis Plus

##### 引入依赖

> 有mybatis plus的启动器就不需要mybatis的启动器了

![1706851513582](image/springboot/1706851513582.png)

##### lombok简化getter/setter

![1706852210427](image/springboot/1706852210427.png)

##### 修改dao

![1706851683376](image/springboot/1706851683376.png)

##### 测试方法

![1706852931948](image/springboot/1706852931948.png)

##### 内置增删改查

![1706860560606](image/springboot/1706860560606.png)

![1706860584866](image/springboot/1706860584866.png)

![1706860599939](image/springboot/1706860599939.png)

![1706860622122](image/springboot/1706860622122.png)
