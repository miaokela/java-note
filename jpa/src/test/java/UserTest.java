import com.miaokela.Application;
import com.miaokela.UserRepository;
import com.miaokela.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Application.class)
public class UserTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testSave() {
        User user = new User();
        user.setName("test");
        user.setEmail("test");
        userRepository.save(user);
    }
}
