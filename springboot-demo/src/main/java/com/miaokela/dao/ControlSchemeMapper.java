package com.miaokela.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.miaokela.entity.ControlScheme;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ControlSchemeMapper extends BaseMapper<ControlScheme> {
}
