package com.miaokela.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "control_scheme")
public class ControlScheme implements Serializable {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    @TableField(value = "protocol_type")
    private Integer protocolType;
    private String config;
    @TableField(value = "controller_type")
    private Integer controllerType;
    @TableField(value = "equipment_id")
    private String equipementId;
}
