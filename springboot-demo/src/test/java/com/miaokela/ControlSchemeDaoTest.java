package com.miaokela;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.miaokela.dao.ControlSchemeMapper;
import com.miaokela.entity.ControlScheme;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ControlSchemeDaoTest {

    @Autowired
    private ControlSchemeMapper controlSchemeMapper;

    @Test
    public void testSelect() {
        List<ControlScheme> controlSchemes = controlSchemeMapper.selectList(null);
        controlSchemes.forEach(System.out::println);
    }

    /**
     * 内置新增方式
     */

    @Test
    public void testInsert() {
        ControlScheme controlScheme = new ControlScheme();
        controlScheme.setControllerType(1);
        controlScheme.setName("方案五");
        controlScheme.setConfig("<root><param name=\"serial_port\">/dev/cu.Bluetooth-Incoming-Port</param><param name=\"baud_rate\">9600</param><param name=\"data_bit\">8</param><param name=\"parity_bit\">0</param><param name=\"stop_bit\">1</param></root>");
        controlScheme.setProtocolType(1);
        controlScheme.setEquipementId("a36f9088926f11eea3a7e45f0153b5f6");

        Assert.assertTrue(controlSchemeMapper.insert(controlScheme) > 0);
    }

    /**
     * 内置删除方式
     */
    @Test
    public void testDelete() {
        // 根据主键ID删除
        controlSchemeMapper.deleteById(3);

        // 批量删除
        controlSchemeMapper.delete(new QueryWrapper<ControlScheme>().like("name", "四"));
        controlSchemeMapper.delete(Wrappers.<ControlScheme>query().like("name", "三"));
        controlSchemeMapper.delete(Wrappers.<ControlScheme>query().lambda().like(ControlScheme::getName, "五"));
    }

    /**
     * 内置修改方式
     */
    @Test
    public void testUpdate() {
        ControlScheme newControlScheme = new ControlScheme();
        newControlScheme.setId(1L);
        newControlScheme.setName("方案一");
        // 基本修改
        controlSchemeMapper.updateById(newControlScheme);

        // 批量修改
        controlSchemeMapper.update(null, Wrappers.<ControlScheme>update().set("name", "方案六").like("name", "二"));

        ControlScheme newControlScheme2 = new ControlScheme();
        newControlScheme2.setName("方案二");
        controlSchemeMapper.update(newControlScheme2, Wrappers.<ControlScheme>update().like("name", "方案六"));
    }

    /**
     * 内置查询
     */
    @Test
    public void testSelect2() {
        // 基本查询
        ControlScheme controlScheme = controlSchemeMapper.selectOne(Wrappers.<ControlScheme>query().eq("name", "方案二"));
        System.out.println(controlScheme.getName());

        // 投影查询
        controlSchemeMapper.selectList(new QueryWrapper<ControlScheme>().select("id", "name")).forEach(
                controlScheme1 -> System.out.println(controlScheme1)
        );
    }

}
