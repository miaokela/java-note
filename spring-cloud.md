## Spring Cloud开发流程

### 完整体系架构

![1707064764267](image/spring-cloud/1707064764267.png)

### 创建父工程

> 统一管理子工程的版本和配置

##### 父工程pom

![1706938015995](image/spring-cloud/1706938015995.png)

![1706938028861](image/spring-cloud/1706938028861.png)

### 创建服务提供者

##### 添加依赖

![1706939512779](image/spring-cloud/1706939512779.png)

##### 配置application.yml

![1706939660219](image/spring-cloud/1706939660219.png)

##### 编写启动类

![1706941435525](image/spring-cloud/1706941435525.png)

##### 编写实体类

![1706942297646](image/spring-cloud/1706942297646.png)

##### 编写mapper

![1706941861331](image/spring-cloud/1706941861331.png)

##### 编写service

![1706940645794](image/spring-cloud/1706940645794.png)

##### 编写controller

![1706940656743](image/spring-cloud/1706940656743.png)

##### 启动并测试

> 访问 http://localhost:9001/user/2

![1706943796696](image/spring-cloud/1706943796696.png)

### 创建服务调用者

##### 添加依赖

![1706942888896](image/spring-cloud/1706942888896.png)

##### 编写启动类

![1706943091467](image/spring-cloud/1706943091467.png)

添加实体类

![1706943178592](image/spring-cloud/1706943178592.png)

##### 编写controller

![1706943415456](image/spring-cloud/1706943415456.png)

##### 启动测试

> 访问 http://localhost:8080/consumer/2

![1706943808546](image/spring-cloud/1706943808546.png)

### 分布式服务面对的问题

```
服务管理
	如何自动注册和发现
	如何实现状态监管
	如何实现动态路由
服务如何实现负载均衡
服务如何解决容灾问题
服务如何实现统一配置
```

### Eureka注册中心

> 服务注册中心，对外暴露自己的地址

> 提供者：向Eureka注册自己的信息

> 消费者：向Eureka订阅服务，Eureka将提供者列表发送给消费者

> 心跳：提供者定期通过HTTP方式向Eureka刷新自己的状态

##### 搭建Eureka服务

###### 添加依赖

![1706944752487](image/spring-cloud/1706944752487.png)

###### 编写启动类

![1706944862215](image/spring-cloud/1706944862215.png)

###### 编写配置

![1706945071107](image/spring-cloud/1706945071107.png)

###### 启动测试

![1707010958317](image/spring-cloud/1707010958317.png)

##### 服务注册

###### 添加依赖

> service-provider添加依赖

![1707011263324](image/spring-cloud/1707011263324.png)

###### 启动类添加注解

![1707011984142](image/spring-cloud/1707011984142.png)

###### 编写配置

![1707012103140](image/spring-cloud/1707012103140.png)

###### 重启service-provider查看注册情况

![1707012197208](image/spring-cloud/1707012197208.png)

##### 服务发现

###### 添加依赖

> service-consumer 添加依赖

![1707012497469](image/spring-cloud/1707012497469.png)

###### 修改配置

![1707012590992](image/spring-cloud/1707012590992.png)

###### 启动类添加注解

![1707012774824](image/spring-cloud/1707012774824.png)

###### 获取服务实例

![1707013728726](image/spring-cloud/1707013728726.png)

###### 重启服务测试服务获取

![1707013604018](image/spring-cloud/1707013604018.png)

![1707013758742](image/spring-cloud/1707013758742.png)

##### Eureka集群

> 简单带过

![1707014046808](image/spring-cloud/1707014046808.png)

![1707014068406](image/spring-cloud/1707014068406.png)

![1707014117705](image/spring-cloud/1707014117705.png)

##### Eureka客户端工程配置

###### 服务注册

![1707014389462](image/spring-cloud/1707014389462.png)

###### 服务续约

![1707014419667](image/spring-cloud/1707014419667.png)

###### 获取服务列表

![1707014446566](image/spring-cloud/1707014446566.png)

Eureka服务端配置

> 失效剔除和自我保护

![1707014550510](image/spring-cloud/1707014550510.png)

### 负载均衡Ribbon

##### 开启负载均衡

![1707014868112](image/spring-cloud/1707014868112.png)

##### 配置负载均衡策略

![1707014931064](image/spring-cloud/1707014931064.png)

### Histrix

> 解决雪崩的手段：线程隔离、服务降级

##### 原理

```
1 为每个服务调用分配一个小的线程池，如果线程池已满，立即拒绝，加速失败判断
2 服务降级
```

##### 触发情况

```
1 线程池已满
2 请求超时
```

##### 引入依赖

![1707015665309](image/spring-cloud/1707015665309.png)

开启熔断器注解

![1707015717544](image/spring-cloud/1707015717544.png)

> 简化注解

![1707015800869](image/spring-cloud/1707015800869.png)

##### 服务降级

> 注意：降级方法与正常逻辑方法保证相同参数列表与返回值声明

![1707016641254](image/spring-cloud/1707016641254.png)

![1707016666154](image/spring-cloud/1707016666154.png)

##### 默认fallback

> 在类上统一默认fallback

![1707018073743](image/spring-cloud/1707018073743.png)

##### 超时配置

![1707018224895](image/spring-cloud/1707018224895.png)

##### 熔断原理

![1707018401992](image/spring-cloud/1707018401992.png)

##### 修改熔断策略

![1707018520967](image/spring-cloud/1707018520967.png)

### Feign

> 远程HTTP服务调用

##### 引入依赖

![1707019575833](image/spring-cloud/1707019575833.png)

##### 编写Feign客户端

> Feign会通过动态代理生成实现类

![1707019753519](image/spring-cloud/1707019753519.png)

##### 开启Feign功能

![1707019967492](image/spring-cloud/1707019967492.png)

##### 修改控制器

![1707020086105](image/spring-cloud/1707020086105.png)

##### 请求测试

![1707020168305](image/spring-cloud/1707020168305.png)

##### Ribbon支持

![1707020335976](image/spring-cloud/1707020335976.png)

###### 配置Ribbon请求超时时间

![1707020461407](image/spring-cloud/1707020461407.png)

> 指定某个服务的超时时长

![1707020499084](image/spring-cloud/1707020499084.png)

> 重试机制配置

![1707020592727](image/spring-cloud/1707020592727.png)

##### Hystrix支持

> 默认情况下是关闭的

![1707020676921](image/spring-cloud/1707020676921.png)

###### 开启Hystrix的配置

![1707020793028](image/spring-cloud/1707020793028.png)

###### Fallback配置

> 实现Feign客户端接口

![1707021383286](image/spring-cloud/1707021383286.png)

> 指定实现类

![1707021442828](image/spring-cloud/1707021442828.png)

###### 请求测试

![1707021510263](image/spring-cloud/1707021510263.png)

##### 请求压缩

![1707041609098](image/spring-cloud/1707041609098.png)

##### 日志级别

![1707041785611](image/spring-cloud/1707041785611.png)

> Feign配置类

![1707042028696](image/spring-cloud/1707042028696.png)

> @FeignCilent注解中指定配置类

![1707042186093](image/spring-cloud/1707042186093.png)

### Spring Cloud Gateway网关

##### 新建工程

##### 引入依赖

![1707046294480](image/spring-cloud/1707046294480.png)

##### 编写启动类

![1707046354261](image/spring-cloud/1707046354261.png)

##### 编写配置

> 断言映射的路径都会代理到uri中

![1707048443105](image/spring-cloud/1707048443105.png)

##### 启动测试

![1707048390631](image/spring-cloud/1707048390631.png)

##### 路由前缀

> 添加前缀

![1707048685326](image/spring-cloud/1707048685326.png)

> 删除前缀

![1707048768873](image/spring-cloud/1707048768873.png)

##### 过滤器

###### 全局默认过滤器

![1707049389541](image/spring-cloud/1707049389541.png)

![1707049354625](image/spring-cloud/1707049354625.png)

###### 局部过滤器

> 需要通过配置文件指定，实现GatewayFilterFactory接口

![1707053240007](image/spring-cloud/1707053240007.png)

![1707053260126](image/spring-cloud/1707053260126.png)

###### 全局过滤器

> 不需要配置，实现GlobalFilter接口

![1707053999855](image/spring-cloud/1707053999855.png)

![1707053896024](image/spring-cloud/1707053896024.png)

![1707053977647](image/spring-cloud/1707053977647.png)

###### 默认集成负载均衡和熔断

![1707054787532](image/spring-cloud/1707054787532.png)

###### 跨域配置

![1707055469475](image/spring-cloud/1707055469475.png)

###### 高可用

> 使用Nginx代理

### Spring Cloud Config 分布式配置中心

##### 简介

![1707055763933](image/spring-cloud/1707055763933.png)

##### 创建远程仓库

> 使用码云存放配置文件

![1707056495971](image/spring-cloud/1707056495971.png)

##### 创建配置文件

> 命名方式: {application}-{env}.yml

![1707064306878](image/spring-cloud/1707064306878.png)

##### 搭建配置中心微服务

###### 创建项目添加依赖

![1707057880821](image/spring-cloud/1707057880821.png)

###### 修改启动类

![1707057980713](image/spring-cloud/1707057980713.png)

###### 修改配置文件

![1707058092403](image/spring-cloud/1707058092403.png)

###### 启动测试

![1707058622850](image/spring-cloud/1707058622850.png)

###### 获取配置中心配置

> 添加依赖

![1707059313063](image/spring-cloud/1707059313063.png)

> 删除 service-provider服务的配置文件application.yml

> 创建 service-provider服务的bootstrap.yml配置文件

![1707059620609](image/spring-cloud/1707059620609.png)


### Spring Cloud Bus消息总线

##### 创建RabbitMQ容器

```
docker run -d --name rabbit -e \
RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin \
-p 15672:15672 \
-p 5672:5672 \
-p 25672:25672 \
-p 61613:61613 \
-p 1883:1883 rabbitmq:management
```

##### 引入依赖

![1707063281474](image/spring-cloud/1707063281474.png)

##### 修改配置

![1707063523165](image/spring-cloud/1707063523165.png)

##### 修改用户服务

> 添加依赖

![1707063756010](image/spring-cloud/1707063756010.png)

> 修改service-provider的配置

![1707064022913](image/spring-cloud/1707064022913.png)

> 修改controller

![1707064418705](image/spring-cloud/1707064418705.png)

##### 测试

> 请求: http://127.0.0.1:12001/actuator/bus-refresh

```
访问配置中心的消息总线服务，消息总线接收到请求后向rabbitmq发送消息，微服务监听消息队列后，重新从配置中心获取最新的配置信息
```
