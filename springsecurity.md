### 引入依赖

![1707223539647](image/springsecurity/1707223539647.png)

### 编写JWT工具类

![1707229024046](image/springsecurity/1707229024046.png)

### 过滤器中解析JWT

> 创建JwtAuthenticationTokenFilter类，继承OncePerRequestFilter

![1707229066286](image/springsecurity/1707229066286.png)

### 加载用户信息

> 实现UserDetailsService接口

![1707229237245](image/springsecurity/1707229237245.png)

### 配置JWT过滤器

> 设置不需要认证的路径，配置JWT登录过滤器

![1707229403412](image/springsecurity/1707229403412.png)

### 编写Controller

![1707229661588](image/springsecurity/1707229661588.png)

> 实体类

![1707229686593](image/springsecurity/1707229686593.png)

![1707229698234](image/springsecurity/1707229698234.png)

### 请求测试

![1707229736065](image/springsecurity/1707229736065.png)
